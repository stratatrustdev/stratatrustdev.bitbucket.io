var OpenWeatherMapWidget = (function() {
  var appId = 'd64fa6dfcab6c228c6fd340ebca2b4ee',
      $errors,
      $weather,
      $cur,
      $curLoc,
      $curTemps,
      $curTemp,
      $curTempMax,
      $curTempMin,
      $curCond,
      $curHum,
      $curWind,
      $forecast;

  function OpenWeatherMapWidget(selector, city) {
    $errors = $('.errors');
    $weather = $('.weather');
    $cur = $weather.find('.cur');
    $curLoc = $cur.find('.loc');
    $curTemps = $cur.find('.temps');
    $curTemp = $curTemps.find('.temp-val');
    $curTempMax = $curTemps.find('.max-val');
    $curTempMin = $curTemps.find('.min-val');
    $curCond = $cur.find('.cur-cond');
    $curHum = $cur.find('.cur-hum').find('.val');
    $curWind = $cur.find('.cur-wind').find('.val');
    $forecast = $weather.find('.forecast');
    if (city) { $curLoc.val(city); }
    
    $curLoc.on('change', function() { this.getWeather(); }.bind(this));
  }
  
  OpenWeatherMapWidget.prototype = {
    getWeather: function() {
      var forecast = [];
      
      resizeInput($curLoc);
      
      var getCurrentWeather = $.ajax({
        dataType: 'json',
        url: 'http://api.openweathermap.org/data/2.5/weather',
        data: {
          q: $curLoc.val().trim(),
          units: 'imperial',
          appid: appId
        }
      });

      var getForecast = $.ajax({
        dataType: 'json',
        url: 'http://api.openweathermap.org/data/2.5/forecast',
        data: {
          q: $curLoc.val().trim(),
          units: 'imperial',
          appid: appId
        }
      });

      getCurrentWeather.done(function(data) {
        $curLoc.val(data.name);
        $curTemp.html(Math.round(data.main.temp));
        $curTempMax.html(Math.round(data.main.temp_max));
        $curTempMin.html(Math.round(data.main.temp_min));
        $curHum.html(data.main.humidity);
        $curWind.html(data.wind.speed);
        $curCond.empty();
        
        data.weather.forEach(function(w) {
          var d = document.createElement('div');
          d.className = 'weather__cond';
          var s = document.createElement('div');
          s.className = 'weather__cond-desc'
          s.innerHTML = w.description.toProperCase();
          var i = document.createElement('i');
          var iClass = 'wi wi-owm-' + w.id;
          i.className = iClass;
          d.append(i);
          d.append(s);
          $curCond.append(d);
        });
      });

      $forecast.empty();
      
      getForecast.done(function(data) {
        var forecasts = aggregateForecasts(data.list.map(d => parseForecastData(d)));
        forecasts.forEach(f => {
          var d = document.createElement('div');
          var temp = document.createElement('div');
          var temps = document.createElement('div');
          var minmax = document.createElement('div');
          var min = document.createElement('div');
          var max = document.createElement('div');
          var weather = document.createElement('i');
          var day = document.createElement('div');
          
          d.className = 'cast';
          
          temp.innerHTML = f.avgTemp;
          temp.className = 'temp-val';
          
          temps.className = 'temps';
          
          minmax.className = 'minmax';
          
          min.innerHTML = f.minLow;
          min.className = 'min';
          
          max.innerHTML = f.maxHigh;
          max.className = 'max';
          
          weather.className = 'wi wi-owm-' + f.weather.id;
          
          day.className = 'day';
          day.innerHTML = f.day;

          minmax.append(min);
          minmax.append(max);

          //temps.append(temp);
          temps.append(minmax);
          
          
          d.append(weather);
          d.append(temps);
          d.append(day);
          
          $forecast.append(d);
        });
      });
    }
  };

  function aggregateForecasts(d) {
    var now = (new Date(Date.now())).toLocaleDateString().split('/');
    var today = pad(now[2], 4) + '-' + pad(now[0], 2) + '-' + pad(now[1], 2);
    var distinctDates = d.distinct('dt');
    var forecasts = [];

    distinctDates.filter(x => x !== today).forEach(x => {
      var matchingDates = d.filter(f => f.dt === x);
      var minLow = Math.min(...matchingDates.map(l => l.tMin));
      var maxHigh = Math.max(...matchingDates.map(l => l.tMax));
      var avgTemp = (minLow + maxHigh) / 2;
      var weatherId = matchingDates.map(w => w.weather).mode('id')[0];
      var weather = matchingDates.filter(w => w.id === weatherId)[0];
      
      forecasts.push({
        dt: x,
        day: matchingDates[0].day,
        avgTemp: Math.round(avgTemp),
        minLow: Math.round(minLow),
        maxHigh: Math.round(maxHigh),
        weather: weather.weather
      });
    });
    return forecasts;
  }
  
  function parseForecastData(d) {

    return {
      dt:  d.dt_txt.split(' ')[0],
      day: new Date(d.dt_txt).toString().split(' ')[0],
      temp: d.main.temp,
      tMax: d.main.temp_max,
      tMin: d.main.temp_min,
      weather: d.weather[0]
    };
  }

  function resizeInput(i) {
    $(i).attr('size', $(i).val().length);
  }
  
  return OpenWeatherMapWidget;
})();

function pad(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

// proper case string prptotype (JScript 5.5+)
String.prototype.toProperCase = function() {
  return this.toLowerCase().replace(/^(.)|\s(.)/g, function($1) { return $1.toUpperCase(); });
}

Array.prototype.distinct = function(key) {
  return [...new Set(this.map(x => x[key]))];
}

Array.prototype.mode = function(key) {
if(this.length == 0) { return null; }

var modeMap = {};
var maxEl = this[0][key], maxCount = 1;
for(var i = 0; i < this.length; i++)
{
    var el = this[i][key];
    if(modeMap[el] == null)
        modeMap[el] = 1;
    else
        modeMap[el]++;
        
    if(modeMap[el] > maxCount)
    {
        maxEl = el;
        maxCount = modeMap[el];
    }
}
return maxEl;
}