var SlideShow = (function() {
    var url,
        imageContainer,
        images = [],
        interval;

    function SlideShow(source, imageContainerSelector, timeoutInterval) {
        url = source;
        imageContainer = $(imageContainerSelector);
        interval = timeoutInterval;

        // do it
        this.run();
    }

    SlideShow.prototype = {
        load: function() {
            $.ajax({
                async: false,
                url: url,
                success: function (data) {
                    console.log('content reload from ' + url + ' successful');
                    console.log(data);
                    images = [];
                    for (var i = 0; i < data.values.length; i++) {
                        var filename = '<img src="https://stratatrustdev.bitbucket.io/' + data.values[i].path + '"/>';
                        images.push(filename);
                    }
                    //images.shuffle();
                }
            }).done(function() { return; });
        },
        play: function() {
            var index,
                length;

            var retryInterval = setInterval(function() {
                this.load();
                index = 0;
                length = images.length;

                if (images.length) {
                    //images.shuffle();
                    clearInterval(retryInterval);
                }

            }.bind(this), 1000);

            // // show first image and increment
            imageContainer.html(images[0]);
            index++;

            // periodically show the rest.  shuffle when the loop is complete.
            setInterval(function() {
                if (index >= length) {
                    index = 0;
                    length = images.length;
                    this.load();
                    //images.shuffle();
                }
                imageContainer.html(images[index]);
                index++;
            }.bind(this), interval);
        },
        run: function() {
            this.load();
            this.play();
        }
    };

    return SlideShow;
})();

Array.prototype.shuffle = function() {
    // Fisher-Yates (aka Knuth) Shuffle.
    var currentIndex = this.length,
        temporaryValue,
        randomIndex;
  
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
  
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
  
      // And swap it with the current element.
      temporaryValue = this[currentIndex];
      this[currentIndex] = this[randomIndex];
      this[randomIndex] = temporaryValue;
    }
  
    return this;
}