(function() {

    function resizeHandler(event) {
        setBgHeight();
    }

    function setBgHeight() {
      let bg = $('.bg');
      bg.each(function() {
        let t = $(this);
        let imageHeight = t.find('img').height();
        let bodyHeight = parseInt(window.innerHeight);
        t.height(Math.min(imageHeight, bodyHeight));
      });
    }

    window.addEventListener('resize', function(event) {
        resizeHandler(event);
    }, false);

    setBgHeight();
})();
