var Clock = function () {
    var clock, date, time;

      function Clock(clockSelector) {
        clock = document.querySelector(clockSelector);
        date = clock.querySelector('.clock__date');
        time = clock.querySelector('.clock__time');
      }
      Clock.prototype = {
        run: function run() {
          setInterval(function () {
            update();
          }, 1000);
        }
      };

      function update() {
        var dt = moment();
        date.textContent = dt.format('MMM Do YYYY');
        time.textContent = dt.format('hh:mm:ss a');
      }

      return Clock;
    }();

    var clock = new Clock('#clock');
    clock.run();